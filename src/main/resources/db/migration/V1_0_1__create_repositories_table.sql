create table mercury.repositories
(
    id          BIGSERIAL PRIMARY KEY,               -- Primary key, auto-incrementing
    group_name  VARCHAR(255) NOT NULL,               -- Assuming a reasonable max length for the group name
    name        VARCHAR(255) NOT NULL,               -- Assuming a reasonable max length for the name
    description TEXT,                                -- Description can be longer, so TEXT is suitable
    created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP, -- For storing date and time
    updated_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP, -- Auto-update timestamp
    web_url     VARCHAR(2048),                       -- URL max length can be large, adjusted to common standards
    visibility  VARCHAR(50),                         -- Assuming visibility is stored as a string (e.g., 'public', 'private')
    latest_tag  VARCHAR(255),                        -- Assuming reasonable max length for tags
    language    VARCHAR(100)                         -- Assuming a reasonable max length for language
);