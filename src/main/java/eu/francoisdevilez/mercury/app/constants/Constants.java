/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.mercury.app.constants;

public interface Constants {
  String DATE_CONSTANT = "Date";
  String STATUS_CONSTANT = "Status";

  String PAUSE_CONSTANT = "Pause";
  String ICE_BOX_CONSTANT = "Ice Box";
  String IN_PROGRESS_CONSTANT = "In Progress";
  String DONE_CONSTANT = "Done";
  String ARCHIVED_CONSTANT = "Archived";
}
