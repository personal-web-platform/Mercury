/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.mercury.jobs.notion;

import static eu.francoisdevilez.mercury.app.constants.Constants.*;

import eu.francoisdevilez.mercury.configurations.NotionConfiguration;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import notion.api.v1.NotionClient;
import notion.api.v1.model.databases.DatabaseProperty;
import notion.api.v1.model.databases.query.filter.PropertyFilter;
import notion.api.v1.model.databases.query.filter.condition.SelectFilter;
import notion.api.v1.model.pages.PageProperty;
import notion.api.v1.request.databases.QueryDatabaseRequest;
import notion.api.v1.request.pages.UpdatePageRequest;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class NotionJob2 implements NotionJob {
  private final NotionClient notionClient;
  private final NotionConfiguration notionConfiguration;

  /** Move the done pages into archived column */
  @Override
  public void execute() {
    QueryDatabaseRequest queryDatabaseRequest =
        new QueryDatabaseRequest(
            notionConfiguration.getTodoDatabaseId(),
            new PropertyFilter(
                STATUS_CONSTANT,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                new SelectFilter(DONE_CONSTANT, null, null, null),
                null));

    notionClient
        .queryDatabase(queryDatabaseRequest)
        .getResults()
        .forEach(
            page -> {
              Map<String, PageProperty> map = new HashMap<>();
              PageProperty statusPageProperty = new PageProperty();
              statusPageProperty.setSelect(
                  new DatabaseProperty.Select.Option(null, ARCHIVED_CONSTANT, null, null));

              map.put(STATUS_CONSTANT, statusPageProperty);
              UpdatePageRequest updatePageRequest = new UpdatePageRequest(page.getId(), map);
              notionClient.updatePage(updatePageRequest);
            });
  }
}
