/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.mercury.jobs.notion;

import static eu.francoisdevilez.mercury.app.constants.Constants.*;
import static eu.francoisdevilez.mercury.utils.Utils.getCurrentDayString;

import eu.francoisdevilez.mercury.configurations.NotionConfiguration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import notion.api.v1.NotionClient;
import notion.api.v1.model.databases.query.filter.CompoundFilter;
import notion.api.v1.model.databases.query.filter.PropertyFilter;
import notion.api.v1.model.databases.query.filter.condition.SelectFilter;
import notion.api.v1.model.pages.PageProperty;
import notion.api.v1.request.databases.QueryDatabaseRequest;
import notion.api.v1.request.pages.UpdatePageRequest;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class NotionJob3 implements NotionJob {
  private final NotionClient notionClient;
  private final NotionConfiguration notionConfiguration;

  /** Updates the pages' dates in Ice box and In progress column */
  @Override
  public void execute() {
    QueryDatabaseRequest queryDatabaseRequest =
        new QueryDatabaseRequest(
            notionConfiguration.getTodoDatabaseId(),
            new CompoundFilter(
                List.of(
                    new PropertyFilter(
                        STATUS_CONSTANT,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new SelectFilter(ICE_BOX_CONSTANT, null, null, null),
                        null),
                    new PropertyFilter(
                        STATUS_CONSTANT,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new SelectFilter(IN_PROGRESS_CONSTANT, null, null, null),
                        null)),
                null));

    notionClient
        .queryDatabase(queryDatabaseRequest)
        .getResults()
        .forEach(
            page -> {
              Map<String, PageProperty> map = new HashMap<>();
              PageProperty datePageProperty = new PageProperty();
              datePageProperty.setDate(new PageProperty.Date(getCurrentDayString(), null, null));

              map.put(DATE_CONSTANT, datePageProperty);
              UpdatePageRequest updatePageRequest = new UpdatePageRequest(page.getId(), map);
              notionClient.updatePage(updatePageRequest);
            });
  }
}
