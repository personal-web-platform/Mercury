/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.mercury.jobs.notion;

import static eu.francoisdevilez.mercury.app.constants.Constants.*;
import static eu.francoisdevilez.mercury.utils.Utils.getCurrentDayString;

import eu.francoisdevilez.mercury.configurations.NotionConfiguration;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import notion.api.v1.NotionClient;
import notion.api.v1.model.databases.DatabaseProperty;
import notion.api.v1.model.pages.PageProperty;
import notion.api.v1.request.pages.UpdatePageRequest;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class NotionJob5 implements NotionJob {
  private final NotionClient notionClient;
  private final NotionConfiguration notionConfiguration;

  /** Moves the pro to do page back to Ice Box and update its due date */
  @Override
  public void execute() {
    Map<String, PageProperty> map = new HashMap<>();
    PageProperty datePageProperty = new PageProperty();
    datePageProperty.setDate(new PageProperty.Date(getCurrentDayString(), null, null));
    PageProperty statusPageProperty = new PageProperty();
    statusPageProperty.setSelect(
        new DatabaseProperty.Select.Option(null, ICE_BOX_CONSTANT, null, null));

    map.put(DATE_CONSTANT, datePageProperty);
    map.put(STATUS_CONSTANT, statusPageProperty);

    UpdatePageRequest updatePageRequest =
        new UpdatePageRequest(notionConfiguration.getTodoProPageId(), map);
    notionClient.updatePage(updatePageRequest);
  }
}
