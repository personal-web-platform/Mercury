/*
 * Copyright (C) 2024. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.mercury.jobs.gitlab;

import eu.francoisdevilez.mercury.models.entities.Repository;
import eu.francoisdevilez.mercury.repositories.RepositoriesRepository;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Group;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.Tag;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class GitlabJob1 implements GitlabJob {
  private final RepositoriesRepository repositoriesRepository;
  private final GitLabApi gitLabApi;

  /** Save or update repositories from gitlab in the database */
  @Override
  public void execute() throws Exception {
    for (Group group : gitLabApi.getGroupApi().getGroups(0, 5)) {
      for (Project project : gitLabApi.getGroupApi().getProjects(group.getId())) {
        if (project.getDescription() != null && !project.getDescription().startsWith("Skip ")) {
          List<Tag> tags = gitLabApi.getTagsApi().getTags(project.getId());
          Map<String, Float> language =
              gitLabApi.getProjectApi().getProjectLanguages(project.getId());
          this.saveOrUpdateRepository(
              project,
              group.getName(),
              tags.isEmpty() ? "No tags" : tags.get(0).getName(),
              !language.isEmpty() ? language.keySet().iterator().next() : "");
        }
      }
    }
  }

  private void saveOrUpdateRepository(
      final Project project, final String groupName, final String tag, final String language) {
    repositoriesRepository
        .findByName(project.getName())
        .ifPresentOrElse(
            repository -> {
              if (!repository.getLatestTag().equals(tag)) {
                this.updateRepositoryTag(repository, tag);
              }
            },
            () ->
                this.saveRepository(
                    Repository.builder()
                        .groupName(groupName)
                        .name(project.getName())
                        .description(project.getDescription())
                        .webUrl(project.getWebUrl())
                        .visibility(project.getVisibility().name())
                        .latestTag(tag)
                        .language(language)
                        .build()));
  }

  private void saveRepository(Repository repository) {
    if (repositoriesRepository.findByName(repository.getName()).isEmpty()) {
      log.info("Saving repository: {}", repository);
      repositoriesRepository.save(repository);
    }
  }

  private void updateRepositoryTag(Repository repository, String tag) {
    repositoriesRepository
        .findByName(repository.getName())
        .ifPresent(
            foundRepository -> {
              log.info(
                  "Updating repository: {}, tags {} -> {}",
                  repository.getName(),
                  repository.getLatestTag(),
                  tag);
              foundRepository.setLatestTag(tag);
              repositoriesRepository.save(foundRepository);
            });
  }
}
