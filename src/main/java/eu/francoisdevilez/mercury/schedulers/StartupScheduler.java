/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.mercury.schedulers;

import eu.francoisdevilez.mercury.services.GitlabService;
import eu.francoisdevilez.mercury.services.NotionService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StartupScheduler {
  private final GitlabService gitlabService;
  private final NotionService notionService;

  @EventListener(org.springframework.boot.context.event.ApplicationReadyEvent.class)
  public void runStartupJob() {
    gitlabService.executeAllJobs();
    notionService.executeAllJobs();
  }
}
