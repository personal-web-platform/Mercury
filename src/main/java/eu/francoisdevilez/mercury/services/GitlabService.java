/*
 * Copyright (C) 2024. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.mercury.services;

import eu.francoisdevilez.mercury.jobs.gitlab.GitlabJob;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class GitlabService {
  private final List<GitlabJob> jobs;

  public void executeAllJobs() {
    log.info("Starting gitlab jobs execution");
    jobs.forEach(
        job -> {
          log.info("Executing job: {}", job.getClass().getSimpleName());
          try {
            job.execute();
          } catch (Exception e) {
            log.error("Error executing job: {}", job.getClass().getSimpleName(), e);
          }
        });
    log.info("All gitlab jobs executed");
  }
}
